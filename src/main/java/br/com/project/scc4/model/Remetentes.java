package br.com.project.scc4.model;

import java.io.Serializable;
import java.text.ParseException;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.swing.text.MaskFormatter;
import javax.validation.constraints.Email;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name = "remetentes", uniqueConstraints = {
		@UniqueConstraint(columnNames = "email")
})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Remetentes implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "remetente_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	@Column(nullable = false)
	private String nome;
	
	@Column(nullable = false)
	private String telefone;
	
	@Column(nullable = false)
	private String cpf;
	
	@OneToOne(mappedBy = "remetentes", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Endereco endereco;
	
	@Email
    @Column(nullable = false)
	private String email;
	
	public Remetentes() {
	}
	
	
	public Remetentes(String nome, String telefone, String cpf, Endereco endereco, String email) {
		super();
		this.nome = nome;
		this.telefone = telefone;
		this.cpf = cpf;
		this.endereco = endereco;
		this.email = email;
	}



	/**
	 * Mascara para os campos
	 */
	public static String formatString(String value, String pattern) {
        MaskFormatter mf;
        try {
            mf = new MaskFormatter(pattern);
            mf.setValueContainsLiteralCharacters(false);
            return mf.valueToString(value);
        } catch (ParseException ex) {
            return value;
        }
    }
	
	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	/**
	 * MÉTODOS GETTERS AND SETTERS
	 */
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		if(telefone.length() == 10) {
			telefone = formatString(telefone, "(##) ####-####");
		} else if(telefone.length() == 11) {
			telefone = formatString(telefone, "(##) #####-####");
		}
		this.telefone = telefone;
	}
	
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = formatString(cpf, "###.###.###-##");;
	}
	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	
	public Remetentes converter() {
		return new Remetentes(nome, telefone, cpf, endereco, email);
	}

}
