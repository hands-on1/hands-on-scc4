package br.com.project.scc4.model;

public class NomeBibliografico {
	
	private String nome = "";
	
	
	// MÉTODOS GETTERS AND SETTERS
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome.substring(nome.lastIndexOf(" ")).toUpperCase().trim() + ", " + nome.substring(0, nome.lastIndexOf(" "));
	}
	
}
