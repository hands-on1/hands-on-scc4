package br.com.project.scc4.model;

import java.util.ArrayList;
import java.util.List;

public class ImprimirPares {
	
	List<Integer> numerosPares = new ArrayList<>();

	// MÉTODOS GETTERS AND SETTERS
	public List<Integer> getNumerosPares() {
		return numerosPares;
	}

	public void setNumerosPares(List<Integer> numerosPares) {
		for(Integer num : numerosPares) {
			if(num % 2 == 0) {
				this.numerosPares.add(num);
			}
		}
	}

}
