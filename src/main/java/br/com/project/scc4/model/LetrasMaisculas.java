package br.com.project.scc4.model;

import java.util.ArrayList;
import java.util.List;

public class LetrasMaisculas {
	
	private List<String> listaMaiuscula = new ArrayList<>();

	// MÉTODOS GETTERS AND SETTERS
	public List<String> getListaMaiuscula() {
		return listaMaiuscula;
	}

	public void setListaMaiuscula(List<String> listaMaiuscula) {
		//int posicaoPalavraAteEspaco = 0;
		for(String palavra : listaMaiuscula) {
			if(palavra.contains(" ")) {
				for(int i = 0; i < palavra.length(); i++) {
					if(palavra.substring(i, i+1).equals(" ")) {
						int posicaoPalavraComEspaco = i;
						this.listaMaiuscula.add(palavra.substring(0, posicaoPalavraComEspaco).toUpperCase());
					}
				}
			} else {
				this.listaMaiuscula.add(palavra.toUpperCase());
			}
		}
	}

}
