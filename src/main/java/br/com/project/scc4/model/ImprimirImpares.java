package br.com.project.scc4.model;

import java.util.ArrayList;
import java.util.List;

public class ImprimirImpares {
	
	private List<Integer> numerosImpares = new ArrayList<>();
	
	// MÉTODOS GETTERS AND SETTERS
	public void setNumerosImpares(List<Integer> numerosImpares) {
		for(Integer num : numerosImpares) {
			if(num % 2 != 0) {
				this.numerosImpares.add(num);
			}
		}
	}

	public List<Integer> getNumerosImpares() {
		return numerosImpares;
	}
	

}
