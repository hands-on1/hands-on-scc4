package br.com.project.scc4.model;

import java.util.List;

public class Mensagem {
	
	private String nomeRemetente;
	private List<String> listaDestinatariosMensagem;
	private String mensagemForm;
	
	public String getNomeRemetente() {
		return nomeRemetente;
	}
	public void setNomeRemetente(String nomeRemetente) {
		this.nomeRemetente = nomeRemetente;
	}
	public List<String> getListaDestinatariosMensagem() {
		return listaDestinatariosMensagem;
	}
	public void setListaDestinatariosMensagem(List<String> listaDestinatariosMensagem) {
		this.listaDestinatariosMensagem = listaDestinatariosMensagem;
	}
	public String getMensagemForm() {
		return mensagemForm;
	}
	public void setMensagemForm(String mensagemForm) {
		this.mensagemForm = mensagemForm;
	}
	
	
	

}
