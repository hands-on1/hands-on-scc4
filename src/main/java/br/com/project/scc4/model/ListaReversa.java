package br.com.project.scc4.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ListaReversa {
	
	private List<Integer> numeroReverso = new ArrayList<>();
	
	// MÉTODOS GETTERS AND SETTERS
	public void setNumeroReverso(List<Integer> numeroReverso) {
		this.numeroReverso = numeroReverso;
	}

	public List<Integer> getNumeroReverso() {
		Collections.reverse(numeroReverso);
		return numeroReverso;
	}
	
}
