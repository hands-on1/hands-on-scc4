package br.com.project.scc4.model;

import java.util.ArrayList;
import java.util.List;

public class Consoantes {
	
	List<String> listaConsoantes = new ArrayList<>();
	
	String[] lista = new String[]{ "A","a","E","e","I","i","O","o","U","u"};

	
	// MÉTODOS GETTERS AND SETTERS
	public List<String> getListaConsoantes() {
		return listaConsoantes;
	}

	public void setListaConsoantes(List<String> listaConsoantes) {
		for(String palavra : listaConsoantes) {
			for(int i = 0; i < lista.length; i++) {
				palavra = palavra.replace(lista[i], "");
			}
			this.listaConsoantes.add(palavra);
		}
	}

}
