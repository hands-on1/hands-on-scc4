package br.com.project.scc4.model;

import java.util.ArrayList;
import java.util.List;

public class Vogais {
	
	List<String> listaVogais = new ArrayList<>();
	
	String[] lista = new String[]{ "b","B","c","C","d","D","f","F","g","G","h","H","j","J","k","K","l","L","m","M","n","N","p","P","q","Q","r","R","s","S","t","T","v","V","w","W","x","X","y","Y","z","Z"};

	// MÉTODOS GETTERS AND SETTERS
	public List<String> getListaVogais() {
		return listaVogais;
	}

	public void setListaVogais(List<String> listaVogais) {
		for(String palavra : listaVogais) {
			for (int i = 0; i < lista.length; i++) {
				palavra = palavra.replace(lista[i],"");
			}
			this.listaVogais.add(palavra);
		}
	}
	
	

}
