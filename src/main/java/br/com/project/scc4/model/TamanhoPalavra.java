package br.com.project.scc4.model;

public class TamanhoPalavra {
	
	private String palavra;

	// MÉTODOS GETTERS AND SETTERS
	public String getPalavra() {
		return "Tamanho: " + palavra.length();
	}

	public void setPalavra(String palavra) {
		this.palavra = palavra.substring(0, palavra.indexOf(" "));
	}

}
