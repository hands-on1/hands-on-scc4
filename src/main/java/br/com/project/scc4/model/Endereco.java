package br.com.project.scc4.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "endereco")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Endereco implements Serializable {
	
private static final long serialVersionUID = 1L;

	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

	@Column(nullable = false)
    private String rua;
	
	@Column(nullable = false)
    private String cidade;
	
	@Column(nullable = false)
    private String estado;
	
	@Column(nullable = false)
    private String cep;
	
	@Column(nullable = false)
    private String pais;

    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "remetente_id", nullable = true)
    @JsonIgnore
    private Remetentes remetentes;
    
    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "destinatarios_id", nullable = true)
    @JsonIgnore
    private Destinatarios destinatarios;

    
    public Endereco() {
    }

    public Endereco(String rua, String cidade, String estado, String cep, String pais) {
        this.rua = rua;
        this.cidade = cidade;
        this.estado = estado;
        this.cep = cep;
        this.pais = pais;
        //this.remetentes = remetentes;
    }
      

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRua() {
		return rua;
	}

	public void setRua(String rua) {
		this.rua = rua;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public Remetentes getRemetentes() {
		return remetentes;
	}

	public void setRemetentes(Remetentes remetentes) {
		this.remetentes = remetentes;
	}

	public Destinatarios getDestinatarios() {
		return destinatarios;
	}

	public void setDestinatarios(Destinatarios destinatarios) {
		this.destinatarios = destinatarios;
	}

}
