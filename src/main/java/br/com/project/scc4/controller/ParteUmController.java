package br.com.project.scc4.controller;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.project.scc4.model.Fracao;
import br.com.project.scc4.model.Consoantes;
import br.com.project.scc4.model.ImprimirImpares;
import br.com.project.scc4.model.ImprimirPares;
import br.com.project.scc4.model.LetrasMaisculas;
import br.com.project.scc4.model.ListaReversa;
import br.com.project.scc4.model.NomeBibliografico;
import br.com.project.scc4.model.SistemaMonetario;
import br.com.project.scc4.model.TamanhoPalavra;
import br.com.project.scc4.model.Vogais;

@RestController
@RequestMapping("/parte-1")
public class ParteUmController {
	
	@GetMapping("/listaReversa")
	public List<Integer> listaReversa(@RequestParam("lista") List<Integer> numerosUrl) {
		ListaReversa listaReversa = new ListaReversa();
		listaReversa.setNumeroReverso(numerosUrl);
		return listaReversa.getNumeroReverso();
	}
	
	@GetMapping("/imprimirImpares")
	public List<Integer> listaImpares(@RequestParam("lista") List<Integer> numeros) {
		ImprimirImpares listaImpares = new ImprimirImpares();
		listaImpares.setNumerosImpares(numeros);
		return listaImpares.getNumerosImpares();
	}
	
	@GetMapping("/imprimirPares")
	public List<Integer> listaPares(@RequestParam("lista") List<Integer> numeros) {
		ImprimirPares listaPares = new ImprimirPares();
		listaPares.setNumerosPares(numeros);
		return listaPares.getNumerosPares();
	}
	
	@GetMapping("/tamanho")
	public String tamanhoPalavra(@RequestParam("palavra") String palavra) {
		TamanhoPalavra tamanhoPalavra = new TamanhoPalavra();
		tamanhoPalavra.setPalavra(palavra);
		return tamanhoPalavra.getPalavra();
	}
	
	@GetMapping("/maiusculas")
	public List<String> listaMaiuscula(@RequestParam("palavra") List<String> palavra) {
		LetrasMaisculas letrasMaisculas = new LetrasMaisculas();
		letrasMaisculas.setListaMaiuscula(palavra);
		return letrasMaisculas.getListaMaiuscula();
	}
	
	@GetMapping("/vogais")
	public List<String> listaVogais(@RequestParam("palavra") List<String> palavra) {
		Vogais vogais = new Vogais();
		vogais.setListaVogais(palavra);
		return vogais.getListaVogais();
	}
	
	@GetMapping("/consoantes")
	public List<String> listaConsoantes(@RequestParam("palavra") List<String> palavra) {
		Consoantes consoantes = new Consoantes();
		consoantes.setListaConsoantes(palavra);
		return consoantes.getListaConsoantes();
	}
	
	@GetMapping("/nomeBibliografico")
	public String nomeBibliografico(@RequestParam("nome") String nome) {
		NomeBibliografico nomeBibliografico = new NomeBibliografico();
		nomeBibliografico.setNome(nome);
		return nomeBibliografico.getNome();
	}
	
	@GetMapping("/sistemaMonetario")
	public String SistemaMonetario(@RequestParam("saque") int saque) {
		SistemaMonetario sistemaMonetario = new SistemaMonetario();
		sistemaMonetario.setSaque(saque);
		return sistemaMonetario.getSaque();
	}
	
	@GetMapping("/calculadoraFracao")
	public String CaculadoraFracao() {
		Fracao calc = new Fracao();
		Fracao f1 = new Fracao(1, 4);
        Fracao f2 = new Fracao(4, 5);
        calc.soma(f1, f2);
        //System.out.println(calc.toString());
		//System.out.println(calculadoraFracao.toString());
		return "Simplificada: " + calc.toString() + "<br>" + " Fração: " + calc.getResultadoSimplificado();
	}

}
