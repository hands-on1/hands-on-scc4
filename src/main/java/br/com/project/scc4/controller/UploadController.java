package br.com.project.scc4.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;

import br.com.project.scc4.controller.form.DestinatariosForm;
import br.com.project.scc4.model.Destinatarios;
import br.com.project.scc4.model.Endereco;
import br.com.project.scc4.repository.DestinatariosRepository;

@RestController
@RequestMapping("/destinatarios")
public class UploadController {
	
	@Autowired
	private DestinatariosRepository destinatariosRepository;
	
	@CrossOrigin(origins = "http://localhost:4200")
	@PostMapping("/upload-csv-destinatarios")
	public void uploadCSVFile(@RequestParam("avatar") MultipartFile file, Model model) {
		
		if(file.isEmpty()) {
			model.addAttribute("mensagem", "Por favor, selecione um arquivo CSV para upload.");
			model.addAttribute("status", false);
		} else {
            try (Reader reader = new BufferedReader(new InputStreamReader(file.getInputStream()))) {
                CsvToBean<DestinatariosForm> csvToBean = new CsvToBeanBuilder<DestinatariosForm>(reader)
                		.withType(DestinatariosForm.class)
                        .withIgnoreLeadingWhiteSpace(true)
                        .build();

                List<DestinatariosForm> destinatarios = csvToBean.parse();
                
                for (DestinatariosForm destinatariosForm : destinatarios) {
                	Destinatarios destinatario = new Destinatarios();
					destinatario.setNome(destinatariosForm.getNome());
					destinatario.setTelefone(destinatariosForm.getTelefone());
					destinatario.setCpf(destinatariosForm.getCpf());
					destinatario.setEmail(destinatariosForm.getEmail());
					
					Endereco endereco = new Endereco();
					endereco.setRua(destinatariosForm.getRua());
					endereco.setCidade(destinatariosForm.getCidade());
					endereco.setEstado(destinatariosForm.getEstado());
					endereco.setCep(destinatariosForm.getCep());
					endereco.setPais(destinatariosForm.getPais());
					
					destinatario.setEndereco(endereco);
					destinatario.getEndereco().setDestinatarios(destinatario);
					
					destinatariosRepository.save(destinatario);
					
				}
                

                model.addAttribute("destinatarios", destinatarios);
                model.addAttribute("status", true);
            } catch (Exception ex) {
                model.addAttribute("message", "An error occurred while processing the CSV file.");
                model.addAttribute("status", false);
            }
		}
		
	}

}
