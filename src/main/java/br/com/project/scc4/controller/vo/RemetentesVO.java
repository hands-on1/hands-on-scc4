package br.com.project.scc4.controller.vo;

import java.util.List;
import java.util.stream.Collectors;

import br.com.project.scc4.model.Remetentes;

public class RemetentesVO {
	private Long id;
	private String nome;
	private String telefone;
	private String cpf;
    private String rua;
    private String cidade;
    private String estado;
    private String cep;
    private String pais;
	private String email;
	
	public RemetentesVO(Remetentes remetentes) {
		this.id = remetentes.getId();
		this.nome = remetentes.getNome();
		this.telefone = remetentes.getTelefone();
		this.cpf = remetentes.getCpf();
		this.rua = remetentes.getEndereco().getRua();
		this.cidade = remetentes.getEndereco().getCidade();
		this.estado = remetentes.getEndereco().getEstado();
		this.cep = remetentes.getEndereco().getCep();
		this.pais = remetentes.getEndereco().getPais();
		this.email = remetentes.getEmail();
		
	}

	public Long getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public String getTelefone() {
		return telefone;
	}

	public String getCpf() {
		return cpf;
	}

	public String getRua() {
		return rua;
	}

	public String getCidade() {
		return cidade;
	}

	public String getEstado() {
		return estado;
	}

	public String getCep() {
		return cep;
	}

	public String getPais() {
		return pais;
	}

	public String getEmail() {
		return email;
	}

	public static List<RemetentesVO> converter(List<Remetentes> listaRemetentes) {
		return listaRemetentes.stream().map(RemetentesVO::new).collect(Collectors.toList());
	}
	
}
