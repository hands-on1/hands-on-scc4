package br.com.project.scc4.controller.vo;

import java.util.List;
import java.util.stream.Collectors;

import br.com.project.scc4.model.Destinatarios;
import br.com.project.scc4.model.Remetentes;

public class DestinatariosVO {
	private Long id;
	private String nome;
	private String telefone;
	private String cpf;
    private String rua;
    private String cidade;
    private String estado;
    private String cep;
    private String pais;
	private String email;
	
	public DestinatariosVO(Destinatarios destinatarios) {
		this.id = destinatarios.getId();
		this.nome = destinatarios.getNome();
		this.telefone = destinatarios.getTelefone();
		this.cpf = destinatarios.getCpf();
		this.rua = destinatarios.getEndereco().getRua();
		this.cidade = destinatarios.getEndereco().getCidade();
		this.estado = destinatarios.getEndereco().getEstado();
		this.cep = destinatarios.getEndereco().getCep();
		this.pais = destinatarios.getEndereco().getPais();
		this.email = destinatarios.getEmail();
		
	}

	public Long getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public String getTelefone() {
		return telefone;
	}

	public String getCpf() {
		return cpf;
	}

	public String getRua() {
		return rua;
	}

	public String getCidade() {
		return cidade;
	}

	public String getEstado() {
		return estado;
	}

	public String getCep() {
		return cep;
	}

	public String getPais() {
		return pais;
	}

	public String getEmail() {
		return email;
	}

	public static List<DestinatariosVO> converter(List<Destinatarios> listaDestinatarios) {
		return listaDestinatarios.stream().map(DestinatariosVO::new).collect(Collectors.toList());
	}
	
}
