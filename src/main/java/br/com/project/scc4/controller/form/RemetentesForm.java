package br.com.project.scc4.controller.form;

import com.opencsv.bean.CsvBindByName;

public class RemetentesForm {
	
	@CsvBindByName
	private String nome;
	
	@CsvBindByName
	private String telefone;
	
	@CsvBindByName
	private String cpf;
	
	@CsvBindByName
	private String rua;
	
	@CsvBindByName
	private String cidade;
	
	@CsvBindByName
	private String estado;
	
	@CsvBindByName
	private String cep;
	
	@CsvBindByName
	private String pais;
	
	@CsvBindByName
	private String email;
	
	
	public RemetentesForm(String nome, String telefone, String cpf, String rua, String cidade, String estado,
			String cep, String pais, String email) {
		super();
		this.nome = nome;
		this.telefone = telefone;
		this.cpf = cpf;
		this.rua = rua;
		this.cidade = cidade;
		this.estado = estado;
		this.cep = cep;
		this.pais = pais;
		this.email = email;
	}
	
	/**
	 * Getters and setters 
	 */
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getRua() {
		return rua;
	}
	public void setRua(String rua) {
		this.rua = rua;
	}
	public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	

}
