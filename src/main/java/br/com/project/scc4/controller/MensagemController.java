package br.com.project.scc4.controller;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.project.scc4.controller.vo.DestinatariosVO;
import br.com.project.scc4.model.Destinatarios;
import br.com.project.scc4.model.Mensagem;
import br.com.project.scc4.model.Remetentes;
import br.com.project.scc4.repository.DestinatariosRepository;

@RestController
@RequestMapping("/destinatarios")
public class MensagemController {
	
	@Autowired
	private DestinatariosRepository destinatariosRepository;
	
	public void enviaMensagem(Remetentes remetente, List<Destinatarios> destinatarios) {
		
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping("/listarDestinatarios")
	public List<DestinatariosVO> listaDestinatarios() {
		List<Destinatarios> destinatarios = destinatariosRepository.findAll();
		return DestinatariosVO.converter(destinatarios);
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@PostMapping("/recebeMensagem")
	@Transactional
	public void mensagem(@RequestBody Mensagem mensagem) {		
		for (String destinatario : mensagem.getListaDestinatariosMensagem()) {
			System.out.println(mensagem.getNomeRemetente() + " enviou mensagem para " + destinatario + ": " +  mensagem.getMensagemForm());
		}
	}

}
