package br.com.project.scc4.controller;

import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.project.scc4.controller.form.RemetentesForm;
import br.com.project.scc4.controller.vo.RemetentesVO;
import br.com.project.scc4.exception.BadRequestException;
import br.com.project.scc4.model.Endereco;
import br.com.project.scc4.model.Remetentes;
import br.com.project.scc4.repository.RemetentesRepository;

@RestController
@RequestMapping("/remetentes")
public class RemetentesController {
	
	@Autowired
	private RemetentesRepository remetentesRepository;
	
	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping("/remetente/{id}")
	public RemetentesVO remetentes(@PathVariable("id") long id) {
		Remetentes remetente = remetentesRepository.findById(id);
		return new RemetentesVO(remetente);
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@PostMapping("/cadastrarRemetente")
	@Transactional
	public Remetentes cadastrarRemetente(@RequestBody RemetentesForm cadastrarRemetente) {
		if(remetentesRepository.existsByEmail(cadastrarRemetente.getEmail())) {
			throw new BadRequestException("Endereço de e-mail já está em uso!");
		}
		
		Remetentes remetente = new Remetentes();
		
		remetente.setNome(cadastrarRemetente.getNome());
		remetente.setTelefone(cadastrarRemetente.getTelefone());
		remetente.setCpf(cadastrarRemetente.getCpf());
		remetente.setEmail(cadastrarRemetente.getEmail());
		
		Endereco endereco = new Endereco();
		endereco.setRua(cadastrarRemetente.getRua());
		endereco.setCidade(cadastrarRemetente.getCidade());
		endereco.setEstado(cadastrarRemetente.getEstado());
		endereco.setCep(cadastrarRemetente.getCep());
		endereco.setPais(cadastrarRemetente.getPais());
				
		remetente.setEndereco(endereco);
		remetente.getEndereco().setRemetentes(remetente);
		

		remetentesRepository.save(remetente);
		return remetente;
	}

	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping("/listarRemetentes")
	public List<RemetentesVO> listaRemetentes() {
		List<Remetentes> remetentes = remetentesRepository.findAll();
		return RemetentesVO.converter(remetentes);
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@PutMapping("/editar/{id}")
	@Transactional
	public void editarRemetente(@PathVariable("id") long id, @RequestBody RemetentesForm remetenteEditado) {
		Remetentes remetente = remetentesRepository.findById(id);
		
		if(remetenteEditado.getNome() != null || remetenteEditado.getNome() != "") {
			remetente.setNome(remetenteEditado.getNome());
		}
		
		if(remetenteEditado.getTelefone() != null || remetenteEditado.getTelefone() != "") {
			remetente.setTelefone(remetenteEditado.getTelefone());
		}
		
		if(remetenteEditado.getCpf() != null || remetenteEditado.getCpf() != "") {
			remetente.setCpf(remetenteEditado.getCpf());
		}
		
		if(remetenteEditado.getRua() != null || remetenteEditado.getRua() != "") {
			remetente.getEndereco().setRua(remetenteEditado.getRua());
		}
		
		if(remetenteEditado.getCidade() != null || remetenteEditado.getCidade() != "") {
			remetente.getEndereco().setCidade(remetenteEditado.getCidade());
		}
		
		if(remetenteEditado.getEstado() != null || remetenteEditado.getEstado() != "") {
			remetente.getEndereco().setEstado(remetenteEditado.getEstado());
		}
		
		if(remetenteEditado.getCep() != null || remetenteEditado.getCep() != "") {
			remetente.getEndereco().setCep(remetenteEditado.getCep());
		}
		
		if(remetenteEditado.getPais() != null || remetenteEditado.getPais() != "") {
			remetente.getEndereco().setPais(remetenteEditado.getPais());
		}
		
		if(remetenteEditado.getEmail() != null || remetenteEditado.getEmail() != "") {
			remetente.setEmail(remetenteEditado.getEmail());
		}
		
		remetentesRepository.saveAndFlush(remetente);
		
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@DeleteMapping("/deletar/{id}")
	@Transactional
	public ResponseEntity<?> deletarRemente(@PathVariable("id") Long id) {
		remetentesRepository.deleteById(id);
		return ResponseEntity.ok().build();
	}

}
