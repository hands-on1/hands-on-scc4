package br.com.project.scc4.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.project.scc4.model.Destinatarios;;

public interface DestinatariosRepository extends JpaRepository<Destinatarios, Long> {
	
}
