package br.com.project.scc4.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.project.scc4.model.Remetentes;

public interface RemetentesRepository extends JpaRepository<Remetentes, Long>{

	Remetentes findById(long id);
	
	Boolean existsByEmail(String email);
	
}
